#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>




 int main () {
 int fd, player, puntos, fifo;
 fifo = mkfifo ("/tmp/pipo", 0666);
 if (fifo < 0) {
 perror("FIFO mal creado"); return -1;}
 
 fd = open("/tmp/pipo", O_RDONLY);
 
 while(read(fd, &player, sizeof(player)) == sizeof(player)) {
 
 read(fd, &puntos, sizeof(puntos));
 printf("Jugador %d ha marcado. \nLLeva %d puntos\n", player, puntos);

    if (puntos == 3){
   printf("JUGADOR %d HA GANADO\nQUE MAQUINA\n", player);
   close(fd);
   unlink("/tmp/pipo");
  }
 }
   close(fd);
   unlink("/tmp/pipo");
}
