// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
int countert = 0;
int counter = 0;
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{ 
   
    munmap((void *)datosp, sizeof(DatosMemCompartida));
    
    close(fd);
    
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
        for (int j = 0; j < counter; j++) {
	   esferas[j].Dibuja();
        }
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
 
        Esfera es;
        es = esferas[0];
       
        for (int i = 0;i <= counter;i++)
         {
           if (esferas[i].centro.x < es.centro.x && esferas[i].velocidad.x < 0) es = esferas[i];
         }
        datosp->esfera1 =  es;
        datosp->raqueta1 = jugador1; 
    
        countert++;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
        for(int k = 0;k<paredes.size();k++)
	{
           for(int j = 0; j < counter; j++) {
                paredes[k].Rebota(esferas[j]);
          }
		
		paredes[k].Rebota(jugador1);
		paredes[k].Rebota(jugador2);
          }
        for (int i = 0; i < counter; i++) {
	   esferas[i].Mueve(0.025f);
         	
	if (jugador1.Rebota(esferas[i])) jugador1.counter = 2;
	if (jugador2.Rebota(esferas[i])) jugador2.counter = 2;
	if(fondo_izq.Rebota(esferas[i]))
	{
int I=2;
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
                write(fd, &I, sizeof(int));
		puntos2++;
                write(fd, &puntos2, sizeof(int));
                
                if (puntos2 == 3) exit(0);
	}

	if(fondo_dcho.Rebota(esferas[i]))
	{
int F = 1, comp;
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
                comp = write(fd, &F, sizeof(int)); 
                if (comp < 0){
                 perror("ERROR FIFO"); exit(1);}
            
		puntos1++;
                comp = write(fd, &puntos1, sizeof(int));
              if (comp < 0){
              perror("ERROR FIFO"); exit(1);}

                if (puntos1 == 3) exit(0);
              
	}

        for (int z = 0; z < counter; z++)
          esferas[i].rebote(esferas[z]);

       }

       if (countert > 1000 && counter < MAX_ESFERAS){
           esferas[counter++].Constructor();
           countert = 0;
         }
      else if(countert > 5000 && counter > MAX_ESFERAS)
      countert = 0;
  
      switch (datosp->accion) {
      case -1:OnKeyboardDown('s',0,0); break; 
      case 1:OnKeyboardDown('w',0,0); break;
      case 0:OnKeyboardDown('k',0,0); break; 
      default:break;
   }    
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4; break;
	case 'w':jugador1.velocidad.y=4; break;
	case 'l':jugador2.velocidad.y=-4; break;
	case 'o':jugador2.velocidad.y=4; break;
        case 'k':jugador1.velocidad.y=0;break;
        //case 'ñ':jugador2.velocidad.x=1;break;

	}
}

void CMundo::Init()
{
//Creación mapa memoria
        int fdb;
        fdb = open("/tmp/DatosMem.txt", O_CREAT | O_RDWR , 0600);
         
        if (fdb  < 0){
          perror("Error al abrir"); close(fdb); exit(1);
           }
  
        ftruncate(fdb, sizeof(DatosMemCompartida));

        datosp = (DatosMemCompartida *)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fdb, 0);
         if (datosp == MAP_FAILED) {
          perror("Error en la proyeccion"); close(fdb); exit(1);
}
        close(fdb);
        

//Apertura fifo
       fd = open("/tmp/pipo", O_WRONLY);
        if (fd < 0){
         perror("ERROR FIFO"); exit(1);}

	Plano p;
        esferas[0].Constructor();
        counter++;
 
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
        
         
}
